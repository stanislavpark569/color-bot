from telegram import *
from telegram.ext import *
from functools import reduce, lru_cache
from io import BytesIO
import html
import os
import argparse
import rgbhash

TOKEN = os.getenv('CYKA_TOKEN')


def help(u, c):
    text = f'''
Жми /rgb и получай свой цвет!
Чат: @tovbotn
Код: https://gitlab.com/pviktor/color-bot
'''.strip()

    try:
        u.message.reply_text(text=text, parse_mode=ParseMode.HTML)
    except Exception as ex:
        print(f'Cannot send: {ex}')


def rgbhash(u, c):
    try:
        from_user = u.message.from_user
        nickname = from_user.first_name
        if from_user.last_name:
            nickname += ' ' + from_user.last_name

        image = rgbhash.get_rgb_hash_image(nickname)
        bio = BytesIO()
        bio.name = 'image.jpeg'
        image.save(bio, 'JPEG')
        bio.seek(0)
        c.bot.send_photo(u.message.chat.id,
                         photo=bio, reply_to_message_id=u.message.message_id,
                         caption=f'{nickname}, here is your nickname RGB hash')
    except Exception as e:
        print(e)


def main():
    updater = Updater(TOKEN, use_context=True)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler('rgb', rgbhash))
    dp.add_handler(CommandHandler('start', help))
    dp.add_handler(CommandHandler('help', help))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()

